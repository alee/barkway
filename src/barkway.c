/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "barkway-internal.h"
#include <canterbury/gdbus/canterbury.h>

PopupPrvStruct  *pPopup_prv_struct = NULL;

static void v_popup_service_on_bus_acquired ( GDBusConnection *pConnection, const gchar *pName, gpointer pUser_data );
static void v_popup_service_on_name_acquired( GDBusConnection *pConnection, const gchar *pName, gpointer pUser_data );
static void v_popup_service_on_name_lost    ( GDBusConnection *pConnection, const gchar *pName, gpointer pUser_data );
static void v_popup_state_machine_handler( PopupInfoStruct *pPopupInfoStruct );
static void v_popup_free_popupInfoStruct ( PopupInfoStruct *pPopupInfoStruct );
static gint in_popup_find_value( gpointer pData, gpointer pUserData );
static void v_popup_hide_end( PopupInfoStruct *pPopupInfoStruct );
static gpointer p_popup_service_thread( gpointer data );

static gboolean is_mutex_locked=FALSE;

static void free_data_function(gchar* data)
{
	if(NULL != data)
	{
		g_free(data);
		data = NULL;
	}
}

static void on_notify_current_app_state(CanterburyAppManager *popup_app_mgr_obj , GParamSpec *pspec, gpointer pUserdata)
{
	if((pPopup_prv_struct->pPopupInfo) == NULL && pPopup_prv_struct->pCurrentAppOnTop == NULL)
	{
		return;
	}

	free_data_function(pPopup_prv_struct->pCurrentAppOnTop);

	if(NULL != pPopup_prv_struct->popup_app_mgr_obj)
		pPopup_prv_struct->pCurrentAppOnTop = g_strdup(canterbury_app_manager_get_current_active_app(pPopup_prv_struct->popup_app_mgr_obj));

	if( 0 != g_list_length(pPopup_prv_struct->request_info_list) )
    {
		if(pPopup_prv_struct->pPopupInfo->app_name == NULL)
			return;
		if(g_strcmp0(pPopup_prv_struct->pCurrentAppOnTop,pPopup_prv_struct->pPopupInfo->app_name) != 0)
		{

     		pPopup_prv_struct->pPopupInfo->state_machine_status = POPUP_ENUM_HIDE_START;
			pPopup_prv_struct->current_state  = POPUP_ENUM_HIDE_START;
			if(is_mutex_locked)
			{
				g_mutex_unlock( &pPopup_prv_struct->mutex_lock );
				is_mutex_locked=FALSE;
			}
			v_popup_state_machine_handler(pPopup_prv_struct->pPopupInfo);
		 }
	}
}

/*function to free structure variables */
static void v_popup_free_popupInfoStruct( PopupInfoStruct *pPopupInfoStruct )
{
    POPUP_SERVICE_DEBUG_PRINT("\n %s %d \n",__FUNCTION__,__LINE__ );

    free_data_function((gchar*)pPopupInfoStruct->app_name);
    free_data_function((gchar*)pPopupInfoStruct->title);
    free_data_function((gchar*)pPopupInfoStruct->sound);

   /*if( NULL != pPopupInfoStruct->display_text )
    {
        GVariantIter inIter ;
        gchar *pValue = NULL;
        gchar *pKey = NULL;
        gint inCon = g_variant_iter_init (&inIter, pPopupInfoStruct->display_text);
        g_print("**** 1 %d****",inCon);
        POPUP_SERVICE_DEBUG_PRINT( " \n**** %x\n" , pPopupInfoStruct->display_text );
        while (g_variant_iter_next(&inIter, "{ss}", &pKey, &pValue))
        {
            g_print("pKey = %s pValue= %s \n",pKey,pValue);
            g_free ( pValue );
            g_free ( pKey );
        }
        g_variant_unref(pPopupInfoStruct->display_text);
        pPopupInfoStruct->display_text = NULL;
    }

    if( NULL != pPopupInfoStruct->rows_info )
    {
        POPUP_SERVICE_DEBUG_PRINT( "\n %s %d \n",__FUNCTION__,__LINE__ );
        GVariantIter inIncrementor;
        GVariant *pHashTable = NULL;
        g_variant_iter_init ( &inIncrementor, pPopupInfoStruct->rows_info );
        while ( (pHashTable = g_variant_iter_next_value(&inIncrementor)) != NULL)
        {
            GVariantIter inCrem ;
            gchar *pValue = NULL;
            gchar *pKey = NULL;
            g_variant_iter_init (&inCrem, pHashTable);
            g_print("**** 2****");
            while (g_variant_iter_next(&inCrem, "{ss}", &pKey, &pValue))
            {
                g_print("pKey = %s pValue= %s \n",pKey,pValue);
                g_free ( pValue );
                g_free ( pKey );
            }
            g_variant_unref(pHashTable);
        }
        g_variant_unref(pPopupInfoStruct->rows_info);
        pPopupInfoStruct->rows_info = NULL;
    }

    if( NULL != pPopupInfoStruct->popup_action)
    {
        GVariantIter inCount;
        gchar *pValue = NULL;
        gchar *pKey = NULL;
        g_variant_iter_init (&inCount, pPopupInfoStruct->popup_action);
        g_print("**** 3****");
        while( g_variant_iter_next(&inCount, "{ss}", &pKey, &pValue))
        {
            g_print("pKey = %s pValue= %s \n",pKey,pValue);
            g_free ( pValue );
            g_free ( pKey );
        }
        g_variant_unref(pPopupInfoStruct->popup_action);
        pPopupInfoStruct->popup_action = NULL;
    }

    if( NULL != pPopupInfoStruct->image_data)
    {
        GVariantIter inNum ;
        gchar *pValue = NULL;
        gchar *pKey = NULL;
        g_variant_iter_init (&inNum, pPopupInfoStruct->image_data);
        g_print("**** 4****");
        while (g_variant_iter_next(&inNum, "{ss}", &pKey, &pValue))
        {
            g_free ( pValue );
            g_free ( pKey );
        }
        g_variant_unref(pPopupInfoStruct->image_data);
        pPopupInfoStruct->image_data = NULL;
    }*/

    if(NULL != pPopupInfoStruct)
    {
    	g_free(pPopupInfoStruct);
    	pPopupInfoStruct = NULL;
    }
}

static gboolean g_popup_handle_confirmation_result_list( BarkwayLayer *pObject,
                                                    GDBusMethodInvocation *pInvocation,
                                                    const gchar *pArg_app_name,
                                                    const gchar *const *pArg_confirmation_result,
                                                    gpointer pUser_data )
{
    /*call back function when receive confirmation result from popuplayer */
    POPUP_SERVICE_DEBUG_PRINT(" \n %s %d \n",__FUNCTION__,__LINE__ );
    barkway_layer_complete_confirmation_result_list( pObject, pInvocation );

    barkway_service_emit_confirmation_result_list( pPopup_prv_struct->popup_obj, pArg_app_name,
                                                   pPopup_prv_struct->currentItem_title, pArg_confirmation_result);

    return TRUE;
}

static gboolean g_popup_handle_confirmation_result( BarkwayLayer *pObject,
                                                    GDBusMethodInvocation *pInvocation,
                                                    const gchar *pArg_app_name,
                                                    const gchar *pArg_confirmation_result,
                                                    gint confirmation_value,
                                                    gpointer pUser_data )
{
    /*call back function when receive confirmation result from popuplayer */
    POPUP_SERVICE_DEBUG_PRINT(" \n %s %d \n",__FUNCTION__,__LINE__ );
    barkway_layer_complete_confirmation_result( pObject, pInvocation );

    /*PopupManager *pPluginObject = g_hash_table_lookup(pPopup_prv_struct->module_list , pArg_app_name);
    if(NULL != pPluginObject)
    {
    	plugin_confirmation_result(G_OBJECT (pPluginObject),pArg_app_name,pPopup_prv_struct->currentItem_title,
    								pArg_confirmation_result,confirmation_value);
    	return TRUE ;
    }*/
    barkway_service_emit_confirmation_result( pPopup_prv_struct->popup_obj, pArg_app_name,
                                                   pPopup_prv_struct->currentItem_title, pArg_confirmation_result,confirmation_value);

    /*   complete the show_blocking_popup call else the blocking_show_popup called as sync from application will be in blocked state */
    if(pPopup_prv_struct->pInvocation)
    {
    	barkway_service_complete_show_blocking_popup( pPopup_prv_struct->popup_obj,pPopup_prv_struct->pInvocation,pArg_app_name,
    		pPopup_prv_struct->currentItem_title, pArg_confirmation_result,confirmation_value);
    	g_object_unref(pPopup_prv_struct->pInvocation);
    	pPopup_prv_struct->pInvocation=NULL;
    }

    return TRUE;
}

static gboolean g_popup_handle_error_msg( BarkwayLayer *pObject,
                                          GDBusMethodInvocation *pInvocation,
                                          const gchar *pArg_error_msg,
                                          gpointer pUser_data )
{
    /*call back function when receive error message from popuplayer */
    POPUP_SERVICE_DEBUG_PRINT( "\n %s %d \n", __FUNCTION__, __LINE__ );
    barkway_layer_complete_error_msg( pObject, pInvocation );
    POPUP_SERVICE_DEBUG_PRINT( "%s \n", pArg_error_msg );
    return TRUE;
}

static gboolean g_popup_handle_registration_status( BarkwayLayer *pObject,
                                                    GDBusMethodInvocation *pInvocation,
                                                    gboolean bArg_reg_Status,
                                                    gpointer pUser_data)
{
    /*call back function when receive reg status from popuplayer */
    POPUP_SERVICE_DEBUG_PRINT( "\n %s %d \n",__FUNCTION__,__LINE__ );
    barkway_layer_complete_registration_status( pObject, pInvocation );
    POPUP_SERVICE_DEBUG_PRINT(" bArg_reg_Status = %d \n", bArg_reg_Status );

    if( (TRUE == bArg_reg_Status) )
    {
        pPopup_prv_struct->isLayerRegStatus = TRUE; /* flag for popup-layer Status */
        /* creating thread after getting reg response from popup layer*/

        if(NULL == (g_thread_new("popup_service_thread",p_popup_service_thread, NULL) ))
        {
            /*throw a  warning when thread creation failed*/
            g_warning( "popup service :Thread creation failed \n" );
            return FALSE;
        }
    }
    else
    {
        pPopup_prv_struct->isLayerRegStatus = FALSE;
    }
    return TRUE;
}

static gboolean g_popup_handle_popup_status( BarkwayLayer *pObject,
                                            GDBusMethodInvocation *pInvocation,
                                            const gchar *pArg_app_name,
                                            const gchar *pArg_popup_status,
                                            gpointer pUser_data)
{
    PopupInfoStruct *pPopupInfoStruct = NULL;
    guint inCount = 0;

	POPUP_SERVICE_DEBUG_PRINT( "\n %s %d  items %d pArg_popup_status = %s \n",__FUNCTION__,__LINE__ , g_list_length(pPopup_prv_struct->request_info_list),pArg_popup_status);

    for(inCount=0; inCount<g_list_length(pPopup_prv_struct->request_info_list); inCount++)
    {
        pPopupInfoStruct =(PopupInfoStruct *)g_list_nth_data( pPopup_prv_struct->request_info_list, inCount );
        if(!g_strcmp0(pPopupInfoStruct->title, pPopup_prv_struct->currentItem_title))
        {
            POPUP_SERVICE_DEBUG_PRINT(" struct found in list \n");
            break;
        }
    }

    if( NULL != pPopupInfoStruct)
    {
        /*updating the current and state_machine_status of popup */
        if(!g_strcmp0( pArg_popup_status, "SHOWN" ))
        {
            POPUP_SERVICE_DEBUG_PRINT("its in shown state \n");
            pPopup_prv_struct->current_state = POPUP_ENUM_SHOW_END;
            pPopupInfoStruct->state_machine_status   = POPUP_ENUM_SHOW_END;


        }
        else  if(!g_strcmp0( pArg_popup_status, "HIDDEN" ))
        {
            POPUP_SERVICE_DEBUG_PRINT("its in hide state \n");
            pPopup_prv_struct->current_state = POPUP_ENUM_HIDE_END;
            pPopupInfoStruct->state_machine_status   = POPUP_ENUM_HIDE_END;
        }
        else if(!g_strcmp0( pArg_popup_status, "CANCELLED" ))
        {
        	POPUP_SERVICE_DEBUG_PRINT("its in hide state \n");
        	pPopup_prv_struct->current_state = POPUP_ENUM_CANCELLED;
        	pPopupInfoStruct->state_machine_status   = POPUP_ENUM_CANCELLED;
        }
         /*if status is hide/shown then trigger to state m/c to allow new request  */
        //g_async_queue_push( pPopup_prv_struct->popup_waiting_queue, pPopupInfoStruct );
	v_popup_state_machine_handler(pPopupInfoStruct);
    }
    else
    {
        /*popup info not found in dbList, rel the lock and trigger to state m/c to pop waiting req if any*/
        g_warning(" popup information is not found in list \n ");
        if(is_mutex_locked)
        {
        g_mutex_unlock( &pPopup_prv_struct->mutex_lock );
        is_mutex_locked=FALSE;
        }
        if( 0 != g_list_length(pPopup_prv_struct->request_info_list) )
        {
           pPopupInfoStruct = (PopupInfoStruct *)g_list_nth_data( pPopup_prv_struct->request_info_list, 0 );
           pPopupInfoStruct->state_machine_status = POPUP_ENUM_SHOW_START;
           v_popup_state_machine_handler(pPopupInfoStruct);
        }
    }
    barkway_layer_complete_popup_status( pObject, pInvocation );
    return TRUE;
}

static void v_show_popup_on_demand(PopupInfoStruct *pPopupInfoStruct)
{
	if(NULL == pPopupInfoStruct)
		return;

	pPopup_prv_struct->pPopupInfo = pPopupInfoStruct;

	POPUP_SERVICE_DEBUG_PRINT(" \n request send from popup service to popup layer\n ");

	if( BARKWAY_POPUP_TYPE_ENUM_FOR_OTHER_TYPE == pPopupInfoStruct->popup_type )
	{
		/*sending selection popup show req to popup-layer*/
		barkway_layer_emit_display_selection_popup (pPopup_prv_struct->popup_layer_obj,
														   pPopupInfoStruct->app_name,
														   pPopupInfoStruct->title,
														   pPopupInfoStruct->display_text,
														   pPopupInfoStruct->rows_info,
														   pPopupInfoStruct->image_data);
	}
	else
	{
		/*sending popup show req to popup-layer*/
		barkway_layer_emit_display_popup (pPopup_prv_struct->popup_layer_obj,
											   pPopupInfoStruct->app_name,
											   pPopupInfoStruct->popup_type,
											   pPopupInfoStruct->title,
											   pPopupInfoStruct->display_text,
											   pPopupInfoStruct->popup_action,
											   pPopupInfoStruct->image_data,
											   pPopupInfoStruct->timeout,
											   pPopupInfoStruct->sound,
											   pPopupInfoStruct->voice_control_time);
	}
}

static guint
get_entry_point_type (CbyEntryPointIndex *entry_point_index, const gchar *id)
{
  g_autofree gchar *apertis_type = NULL;
  g_autoptr(CbyEntryPoint) entry_point = NULL;
  GEnumClass *cls;
  GEnumValue *value;

  if (entry_point_index == NULL)
     return CANTERBURY_EXECUTABLE_TYPE_UNKNOWN;

  entry_point = cby_entry_point_index_get_by_id (entry_point_index, id);
  if (entry_point == NULL)
     return CANTERBURY_EXECUTABLE_TYPE_UNKNOWN;

  apertis_type = cby_entry_point_get_string (entry_point, "X-Apertis-Type");
  if (apertis_type == NULL)
     return CANTERBURY_EXECUTABLE_TYPE_UNKNOWN;

  cls = g_type_class_ref (CANTERBURY_TYPE_EXECUTABLE_TYPE);
  value = g_enum_get_value_by_nick (cls, apertis_type);
  g_type_class_unref (cls);
  if (value == NULL)
     return CANTERBURY_EXECUTABLE_TYPE_UNKNOWN;

  return value->value;
}

static void v_popup_show_start( PopupInfoStruct *pPopupInfoStruct )
{
    guint out_app_type = -1;

	if(NULL == pPopupInfoStruct)
	{
		g_warning("Popup Service: critical : Unexpected NULL Pointer @%d \n",__LINE__);
		return ;
	}

    free_data_function((gchar*)pPopup_prv_struct->currentItem_title);
    /*do mutex lock to avoid another show popup till first completes its animation*/
    if(FALSE == is_mutex_locked)
    {
    g_mutex_lock( &pPopup_prv_struct->mutex_lock );
    is_mutex_locked=TRUE;
    }
    pPopupInfoStruct->state_machine_status  = POPUP_ENUM_INITAL;
    pPopup_prv_struct->currentItem_priority = pPopupInfoStruct->priority;
    pPopup_prv_struct->currentItem_title    = g_strdup( pPopupInfoStruct->title );
    pPopup_prv_struct->current_state        = POPUP_ENUM_SHOW_START;

    g_print("%s %d \n", __FUNCTION__ , __LINE__);
    if(NULL != pPopup_prv_struct->entry_point_index)
    {
      out_app_type = get_entry_point_type (pPopup_prv_struct->entry_point_index,
                                           pPopupInfoStruct->app_name);
    }

     
     switch(out_app_type)
     {
		 case CANTERBURY_EXECUTABLE_TYPE_APPLICATION:
		 {
			 g_print("%s %d \n", __FUNCTION__ , __LINE__);
			if( (pPopup_prv_struct->popup_app_mgr_obj != NULL) &&
					(canterbury_app_manager_get_current_active_app(pPopup_prv_struct->popup_app_mgr_obj) !=NULL))
			{
				free_data_function(pPopup_prv_struct->pCurrentAppOnTop);
				pPopup_prv_struct->pCurrentAppOnTop = g_strdup(canterbury_app_manager_get_current_active_app(pPopup_prv_struct->popup_app_mgr_obj));
			}
			if( pPopup_prv_struct->pCurrentAppOnTop!=NULL && g_strcmp0(pPopup_prv_struct->pCurrentAppOnTop,pPopupInfoStruct->app_name)!=0)
			{
				pPopup_prv_struct->request_info_list = g_list_remove( pPopup_prv_struct->request_info_list, pPopupInfoStruct );
				v_popup_free_popupInfoStruct( pPopupInfoStruct );
				pPopupInfoStruct->state_machine_status = POPUP_ENUM_HIDE_END;
				pPopup_prv_struct->current_state = POPUP_ENUM_HIDE_END;
                if(is_mutex_locked)
				{
                	g_mutex_unlock( &pPopup_prv_struct->mutex_lock );
                	is_mutex_locked=FALSE;
				}

				if( 0 != g_list_length(pPopup_prv_struct->request_info_list) )
				{
				   pPopupInfoStruct = (PopupInfoStruct *)g_list_nth_data( pPopup_prv_struct->request_info_list, 0 );
				   pPopupInfoStruct->state_machine_status = POPUP_ENUM_SHOW_START;
				   v_popup_state_machine_handler(pPopupInfoStruct);
				}
			}
			else
			{
				v_show_popup_on_demand(pPopupInfoStruct);
			}
			 break;
		 }
		 case CANTERBURY_EXECUTABLE_TYPE_SERVICE:
		 case CANTERBURY_EXECUTABLE_TYPE_AGENT_SERVICE:
		 case CANTERBURY_EXECUTABLE_TYPE_STARTUP_APPLICATION:
		 case CANTERBURY_EXECUTABLE_TYPE_UNKNOWN:
		 {
			 POPUP_SERVICE_DEBUG_PRINT("in CANTERBURY_EXECUTABLE_TYPE_SERVICE CANTERBURY_EXECUTABLE_TYPE_AGENT_SERVICE  \n");
			 v_show_popup_on_demand(pPopupInfoStruct);
			 break;
		 }
		 case CANTERBURY_EXECUTABLE_TYPE_EXT_APP:
		 {
			 barkway_service_emit_error_msg (pPopup_prv_struct->popup_obj , pPopupInfoStruct->app_name, "Application Type Not found");
			 break;
		 }
		 default:
			 g_warning("Popup Service: Unrecognized Executable Type %d \n",out_app_type);
			 break;
     }
}

static void v_popup_show_end( PopupInfoStruct *pPopupInfoStruct )
{
    guint out_app_type = -1;

	if(NULL == pPopupInfoStruct)
	{
		g_warning("Popup Service: critical : Unexpected NULL Pointer @%d \n",__LINE__);
		return ;
	}

    POPUP_SERVICE_DEBUG_PRINT( "%s \n",__FUNCTION__ );
    barkway_service_emit_popup_status ( pPopup_prv_struct->popup_obj, pPopupInfoStruct->app_name,
                                             pPopup_prv_struct->currentItem_title, "SHOWN");

    /*unlock the thread call after popup animation to allow other request*/
    if(is_mutex_locked)
    {
    	g_mutex_unlock( &pPopup_prv_struct->mutex_lock );
    	is_mutex_locked=FALSE;
    }

    out_app_type = get_entry_point_type (pPopup_prv_struct->entry_point_index,
                                        pPopupInfoStruct->app_name);

       switch(out_app_type)
   	{
   		 case CANTERBURY_EXECUTABLE_TYPE_APPLICATION:
   		 {
   			if((pPopup_prv_struct->pPopupInfo) == NULL && pPopup_prv_struct->pCurrentAppOnTop == NULL)
   			{
   				return;
   			}
   			free_data_function( pPopup_prv_struct->pCurrentAppOnTop);
   			if(NULL != pPopup_prv_struct->popup_app_mgr_obj)
   			{
   				pPopup_prv_struct->pCurrentAppOnTop = g_strdup(
   						canterbury_app_manager_get_current_active_app(pPopup_prv_struct->popup_app_mgr_obj));
   			}

   			if( 0 != g_list_length(pPopup_prv_struct->request_info_list) )
   			{
   				if(pPopup_prv_struct->pPopupInfo->app_name == NULL)
   					return;
   				if(g_strcmp0(pPopup_prv_struct->pCurrentAppOnTop,pPopup_prv_struct->pPopupInfo->app_name) != 0)
   				{
   					pPopup_prv_struct->pPopupInfo->state_machine_status = POPUP_ENUM_HIDE_START;
   					pPopup_prv_struct->current_state  = POPUP_ENUM_HIDE_START;
   					if(is_mutex_locked)
   					{
   						g_mutex_unlock( &pPopup_prv_struct->mutex_lock );
   						is_mutex_locked=FALSE;
   					}
   					v_popup_state_machine_handler(pPopup_prv_struct->pPopupInfo);
   				 }
   			}
   			 break;
   		 }
   		 case CANTERBURY_EXECUTABLE_TYPE_SERVICE:
   		 case CANTERBURY_EXECUTABLE_TYPE_AGENT_SERVICE:
   		 case CANTERBURY_EXECUTABLE_TYPE_STARTUP_APPLICATION:
   		 case CANTERBURY_EXECUTABLE_TYPE_EXT_APP:
   		 case CANTERBURY_EXECUTABLE_TYPE_UNKNOWN:
   		 {
   			POPUP_SERVICE_DEBUG_PRINT(" app_type is service/agent/ext_app \n");
   			 break;
   		 }
		 default:
			 g_warning("Popup Service: Show End :Unrecognized Executable Type %d \n",out_app_type);
			 break;
   	 }
}

static void v_popup_hide_start( PopupInfoStruct *pPopupInfoStruct)
{
	   if(FALSE == is_mutex_locked)
	    {
	    g_mutex_lock( &pPopup_prv_struct->mutex_lock );
	    is_mutex_locked=TRUE;
	    }

    POPUP_SERVICE_DEBUG_PRINT( "%s \n",__FUNCTION__ );

    /*if any hide req is made by app then check if already shown or not if shown hide / else remove from list if it is in queue */
    if(!g_strcmp0( pPopup_prv_struct->currentItem_title, pPopupInfoStruct->title ))
    {
    	POPUP_SERVICE_DEBUG_PRINT( "%s %d \n",__FUNCTION__,__LINE__ );
        pPopup_prv_struct->current_state  = POPUP_ENUM_HIDE_START;
        barkway_layer_emit_hide_popup ( pPopup_prv_struct->popup_layer_obj, pPopupInfoStruct->app_name, pPopupInfoStruct->popup_type );
    }
    else
    {
        GList* pTitle_list = g_list_find_custom(pPopup_prv_struct->request_info_list, pPopupInfoStruct, (GCompareFunc)in_popup_find_value);

        if( 0 != g_list_length(pTitle_list) )
        {
            POPUP_SERVICE_DEBUG_PRINT( "hide popup request found in list, so it is removed \n" );
            pPopup_prv_struct->request_info_list = g_list_remove( pPopup_prv_struct->request_info_list, pPopupInfoStruct );
            v_popup_free_popupInfoStruct( pPopupInfoStruct );
        }
        if(is_mutex_locked)
        {
        	g_mutex_unlock( &pPopup_prv_struct->mutex_lock );
        	is_mutex_locked=FALSE;
        }

        if( 0 != g_list_length(pPopup_prv_struct->request_info_list) )
        {
           pPopupInfoStruct = (PopupInfoStruct *)g_list_nth_data( pPopup_prv_struct->request_info_list, 0 );
           pPopupInfoStruct->state_machine_status = POPUP_ENUM_SHOW_START;
           v_popup_state_machine_handler(pPopupInfoStruct);
        }
    }
}

static void v_popup_hide_end ( PopupInfoStruct *pPopupInfoStruct )
{
    GList *gTitle_list = NULL;

    POPUP_SERVICE_DEBUG_PRINT( "%s %d\n",__FUNCTION__,__LINE__ );
    /*   complete the show_blocking_popup call because if blocking popup is in show state and if any other high priority popup comes into que, then we need to complete the function of blocking popup */
    if(pPopup_prv_struct->pInvocation)
   	 {
   	  barkway_service_complete_show_blocking_popup(pPopup_prv_struct->popup_obj,pPopup_prv_struct->pInvocation,pPopupInfoStruct->app_name,
   			 pPopupInfoStruct->title, "NULL",0);
        	g_object_unref(pPopup_prv_struct->pInvocation);
   	    	pPopup_prv_struct->pInvocation=NULL;
   	 }

    /*removing the info from the infolist after the usage*/
    gTitle_list = g_list_find_custom (pPopup_prv_struct->request_info_list,
        pPopupInfoStruct,
        (GCompareFunc) in_popup_find_value);
    if( 0 != g_list_length( gTitle_list ) )
    {
        barkway_service_emit_popup_status ( pPopup_prv_struct->popup_obj, pPopupInfoStruct->app_name,
                                                 pPopup_prv_struct->currentItem_title, "HIDDEN" );
        POPUP_SERVICE_DEBUG_PRINT( "hide popup request found in list, so it is removed \n" );
        pPopup_prv_struct->request_info_list = g_list_remove( pPopup_prv_struct->request_info_list, pPopupInfoStruct );
        v_popup_free_popupInfoStruct(pPopupInfoStruct);
    }
    if(is_mutex_locked)
    {
    	g_mutex_unlock( &pPopup_prv_struct->mutex_lock );
    	is_mutex_locked=FALSE;
    }

    if(g_list_length(pPopup_prv_struct->request_info_list)!=0)
    {
       pPopupInfoStruct = (PopupInfoStruct *)g_list_nth_data( pPopup_prv_struct->request_info_list, 0 );
       pPopupInfoStruct->state_machine_status = POPUP_ENUM_SHOW_START;
       v_popup_state_machine_handler( pPopupInfoStruct );
    }
}

static void v_popup_wait_state( PopupInfoStruct *pPopupInfoStruct )
{
    POPUP_SERVICE_DEBUG_PRINT("%s \n",__FUNCTION__);
}

/*function to find the title(servers as a unique identifier) in the infolist */
static gint in_popup_find_value(gpointer pData, gpointer pUserData)
{
	PopupInfoStruct *pPopupInf    = (PopupInfoStruct*)pUserData;
    PopupInfoStruct *pPopupStruct = (PopupInfoStruct*)pData;

	if(!g_strcmp0(pPopupInf->title,pPopupStruct->title))
	{
		return 0;
	}
	return 1;
}

/*function to sort and add into infolist */
static gint in_popup_priority_compare_func(gconstpointer pQueueValue ,gconstpointer pNewValue)
{
     PopupInfoStruct *pQueueValue_info =(PopupInfoStruct *)pQueueValue;
     PopupInfoStruct *pNewValue_info   =(PopupInfoStruct *)pNewValue;

     if(pQueueValue_info->priority == pNewValue_info->priority)
     {
         return 0;
     }
     else if(pQueueValue_info->priority < pNewValue_info->priority)
     {
         return 1;
     }
     else
     {
         return -1;
     }
}

static void v_popup_create( PopupInfoStruct *pPopupInfoStruct )
{
    /*popup create is a function where prioritization is done for popup and that info is given to state m/c */
    GList *pTitle_list = g_list_find_custom(pPopup_prv_struct->request_info_list, pPopupInfoStruct, (GCompareFunc)in_popup_find_value );

    if( 0 == g_list_length( pTitle_list ) )
    {
        POPUP_SERVICE_DEBUG_PRINT( "It is a new query, so added to Glist \n" );
        /*new query info is added into infolist with sorting */
        pPopup_prv_struct->request_info_list = g_list_insert_sorted( pPopup_prv_struct->request_info_list, pPopupInfoStruct, in_popup_priority_compare_func);

        /*popup goes into this condition if no popup is shown already*/
        if( (POPUP_ENUM_INITAL == pPopup_prv_struct->current_state) || (POPUP_ENUM_HIDE_END == pPopup_prv_struct->current_state ))
        {
        	POPUP_SERVICE_DEBUG_PRINT( " %s %d \n",__FUNCTION__,__LINE__ );
            pPopupInfoStruct->state_machine_status = POPUP_ENUM_SHOW_START;
        }

        /*popup goes into this condition and undergo prioritization if any popup is shown */
        if(( (POPUP_ENUM_SHOW_END == pPopup_prv_struct->current_state) ) || (POPUP_ENUM_SHOW_START == pPopup_prv_struct->current_state) )
        {
            /*if shown popup has high prio than requested then it goes to waiting state */
            /*else shown popup will be hidden and req popup is shown*/
            if( pPopup_prv_struct->currentItem_priority >= pPopupInfoStruct->priority )
            {
                /*g_print("%d %d %s %s \n",pPopup_prv_struct->currentItem_priority,pPopupInfoStruct->priority,
                        pPopup_prv_struct->currentItem_title,pPopupInfoStruct->title);*/
                pPopupInfoStruct->state_machine_status = POPUP_ENUM_WAIT_STATE;
            }
            else if( pPopup_prv_struct->currentItem_priority < pPopupInfoStruct->priority )
            {
                guint inCount = 0;

                pPopupInfoStruct->state_machine_status = POPUP_ENUM_WAIT_STATE;
                POPUP_SERVICE_DEBUG_PRINT( "\n %s %d \n",__FUNCTION__,__LINE__ );

                for(inCount=0; inCount<g_list_length(pPopup_prv_struct->request_info_list); inCount++)
                {
                    pPopupInfoStruct = (PopupInfoStruct *)g_list_nth_data(pPopup_prv_struct->request_info_list, inCount );
                    if( !g_strcmp0( pPopupInfoStruct->title, pPopup_prv_struct->currentItem_title ) )
                    {
                	pPopupInfoStruct->state_machine_status = POPUP_ENUM_HIDE_START;
                        break;
                    }
                }
            }
        }
        v_popup_state_machine_handler( pPopupInfoStruct );
    }
}


static void v_popup_cancelled(PopupInfoStruct *pPopupInfoStruct)
{
	GList *gTitle_list = NULL;

	POPUP_SERVICE_DEBUG_PRINT( "%s %d\n",__FUNCTION__,__LINE__ );

	/*   complete the show_blocking_popup call in cancelled state else the blocking_show_popup called as sync from application will be in blocked state   */
	 if(pPopup_prv_struct->pInvocation)
	 {
	 barkway_service_complete_show_blocking_popup(pPopup_prv_struct->popup_obj,pPopup_prv_struct->pInvocation,pPopupInfoStruct->app_name,
			 pPopupInfoStruct->title, "NULL",0);
	       g_object_unref(pPopup_prv_struct->pInvocation);
	     	pPopup_prv_struct->pInvocation=NULL;
	 }

	 /*removing the info from the infolist after the usage*/
	gTitle_list = g_list_find_custom (pPopup_prv_struct->request_info_list,
	    pPopupInfoStruct,
	    (GCompareFunc) in_popup_find_value);

	    if( 0 != g_list_length( gTitle_list ) )
	    {
	        barkway_service_emit_popup_status ( pPopup_prv_struct->popup_obj, pPopupInfoStruct->app_name,
	                                                 pPopup_prv_struct->currentItem_title, "CANCELLED" );
	        POPUP_SERVICE_DEBUG_PRINT( "cancel popup request found in list, so it is removed \n" );
	        pPopup_prv_struct->request_info_list = g_list_remove( pPopup_prv_struct->request_info_list, pPopupInfoStruct );
	        v_popup_free_popupInfoStruct(pPopupInfoStruct);
	    }
	    if(is_mutex_locked)
	    {
	    g_mutex_unlock( &pPopup_prv_struct->mutex_lock );
	    is_mutex_locked=FALSE;
	    }
	    if(g_list_length(pPopup_prv_struct->request_info_list)!=0)
	    {
	    	POPUP_SERVICE_DEBUG_PRINT("555555555v_popup_cancelled 55555555 \n");
	       pPopupInfoStruct = (PopupInfoStruct *)g_list_nth_data( pPopup_prv_struct->request_info_list, 0 );
	       pPopupInfoStruct->state_machine_status = POPUP_ENUM_SHOW_START;
	       v_popup_state_machine_handler( pPopupInfoStruct );
	    }
	    else
	    {
	    	pPopup_prv_struct->current_state = POPUP_ENUM_INITAL;
	    }
}

/*Depending on the Situation the state machine is changed by looking at the current state */
void v_popup_state_machine_handler( PopupInfoStruct *pPopupInfoStruct )
{
    switch( pPopupInfoStruct->state_machine_status )
    {
        case POPUP_ENUM_CREATE:
            v_popup_create( pPopupInfoStruct );
            break;

        case POPUP_ENUM_SHOW_START:
            v_popup_show_start( pPopupInfoStruct );
            break;

        case POPUP_ENUM_SHOW_END:
            v_popup_show_end( pPopupInfoStruct );
            break;

        case POPUP_ENUM_HIDE_START:
            v_popup_hide_start( pPopupInfoStruct );
            break;

        case POPUP_ENUM_HIDE_END:
            v_popup_hide_end( pPopupInfoStruct );
            break;
        case POPUP_ENUM_CANCELLED:
        	v_popup_cancelled( pPopupInfoStruct );
        	break;
        case POPUP_ENUM_WAIT_STATE:
            v_popup_wait_state( pPopupInfoStruct );
            break;
	case POPUP_ENUM_INITAL:
        default:
            g_printerr( "POPUP SERVICE : Wrong state defined \n" );
             break;
    }
}

static void
show_popup_internal_handle (const gchar *pArg_app_name,
                            gint64 inArg_popup_type,
                            const gchar *pArg_title,
                            GVariant *pArg_display_text,
                            const gchar *pArg_priority,
                            GVariant *pArg_popup_action,
                            GVariant *pArg_image_data,
                            gdouble dArg_timeout,
                            const gchar *pArg_sound,
                            gint arg_voice_control_time,
                            gpointer pUser_data )
{
    /* allocating memory for structure and initialized to 0's*/
    PopupInfoStruct *pPopupInfoStruct = g_new0 (PopupInfoStruct, 1);
    /* Building the hash table for display Text */
    GVariantBuilder *pDisplay_text = NULL;
    GVariantIter inCount;
    gchar *pValue = NULL;
    gchar *pKey = NULL;
    GVariantBuilder *pHashValues = NULL;
    GVariantIter inIter;
    GVariantBuilder *pImageValues = NULL;
    GVariantIter inCre;

    pPopupInfoStruct->app_name   = g_strdup( pArg_app_name );
    pPopupInfoStruct->title      = g_strdup( pArg_title );
    pPopupInfoStruct->popup_type = inArg_popup_type ;

	pDisplay_text = g_variant_builder_new( G_VARIANT_TYPE ("a{ss}") );
	g_variant_iter_init ( &inCount, pArg_display_text);
	while( g_variant_iter_next ( &inCount, "{ss}", &pKey, &pValue) )
	{
		 g_variant_builder_add (pDisplay_text, "{ss}", (pKey), (pValue));
		 /*g_print("pKey = %s pValue= %s \n",pKey,pValue);*/
	}
	pPopupInfoStruct->display_text = g_variant_builder_end (pDisplay_text);
    g_variant_builder_unref(pDisplay_text);
	/*g_print( " \n**** %x\n" , pPopupInfoStruct->display_text );*/

    /*priority is checked*/
    if(!g_strcmp0( pArg_priority,"HIGH") )
    {
        pPopupInfoStruct->priority = POPUP_ENUM_HIGH_PRIORITY;
    }
    else if(!g_strcmp0( pArg_priority,"MEDIUM") )
    {
        pPopupInfoStruct->priority = POPUP_ENUM_MEDIUM_PRIORITY;
    }
    else
    {
        pPopupInfoStruct->priority = POPUP_ENUM_LOW_PRIORITY;
    }

	/*Building the hash table for popup action */
	pHashValues = g_variant_builder_new( G_VARIANT_TYPE ("a{ss}") );
	g_variant_iter_init ( &inIter, pArg_popup_action );
	while( g_variant_iter_next(&inIter, "{ss}", &pKey, &pValue) )
	{
		 g_variant_builder_add (pHashValues, "{ss}", (pKey), (pValue));
	}
	pPopupInfoStruct->popup_action = g_variant_builder_end ( pHashValues );
    g_variant_builder_unref( pHashValues );


    /*Building the hash table for images data*/
	pImageValues = g_variant_builder_new( G_VARIANT_TYPE ("a{ss}") );
	g_variant_iter_init( &inCre, pArg_image_data );
	while( g_variant_iter_next (&inCre, "{ss}", &pKey, &pValue) )
	{
		 g_variant_builder_add ( pImageValues, "{ss}", (pKey), (pValue));
	}
	pPopupInfoStruct->image_data = g_variant_builder_end( pImageValues );
    g_variant_builder_unref( pImageValues );


    pPopupInfoStruct->timeout = dArg_timeout;
    pPopupInfoStruct->sound = g_strdup(pArg_sound);
    pPopupInfoStruct->voice_control_time = arg_voice_control_time;
    pPopupInfoStruct->state_machine_status = POPUP_ENUM_CREATE;

    /*Pushes the data into the queue*/
    g_async_queue_push( pPopup_prv_struct->popup_waiting_queue, pPopupInfoStruct );
}


static gboolean g_popup_handle_show_blocking_popup( BarkwayService *pObject,
        GDBusMethodInvocation *pInvocation,
        const gchar *pArg_app_name,
        gint64 inArg_popup_type,
        const gchar *pArg_title,
        GVariant *pArg_display_text,
        const gchar *pArg_priority,
        GVariant *pArg_popup_action,
        GVariant *pArg_image_data,
        gdouble dArg_timeout,
        const gchar *pArg_sound,
        gint pArg_voice_control_time,
        gpointer pUser_data )
{
	POPUP_SERVICE_DEBUG_PRINT( " \n %s %d \n",__FUNCTION__,__LINE__ );

	    POPUP_SERVICE_DEBUG_PRINT(" \n request received from application in popup service \n ");

	    /*check: if queue creation is not done then request is not handled*/
	    if( NULL == pPopup_prv_struct->popup_waiting_queue )
	    {
	        return FALSE;
	    }

	    /*check : whether popup-layer is started or not */
	    if( FALSE == pPopup_prv_struct->isLayerRegStatus)
	    {
	        barkway_service_emit_error_msg (pObject, pArg_app_name, " popup-layer not yet started ");
	        return FALSE;
	    }
	    pPopup_prv_struct->pInvocation=g_object_ref(pInvocation);
	    show_popup_internal_handle(pArg_app_name,inArg_popup_type,pArg_title,pArg_display_text,pArg_priority,pArg_popup_action,
	    		pArg_image_data,dArg_timeout,pArg_sound,pArg_voice_control_time,pUser_data);

	return TRUE;
}


/*callback for show popup,when client requested to service */
static gboolean g_popup_handle_show( BarkwayService *pObject,
                                 GDBusMethodInvocation *pInvocation,
                                 const gchar *pArg_app_name,
                                 gint64 inArg_popup_type,
                                 const gchar *pArg_title,
                                 GVariant *pArg_display_text,
                                 const gchar *pArg_priority,
                                 GVariant *pArg_popup_action,
                                 GVariant *pArg_image_data,
                                 gdouble dArg_timeout,
                                 const gchar *pArg_sound,
                                 gint pArg_voice_control_time,
                                 gpointer pUser_data )
{
    POPUP_SERVICE_DEBUG_PRINT( " \n %s %d \n",__FUNCTION__,__LINE__ );

    POPUP_SERVICE_DEBUG_PRINT(" \n request received from application in popup service \n ");

    /*check: if queue creation is not done then request is not handled*/
    if( NULL == pPopup_prv_struct->popup_waiting_queue )
    {
        return FALSE;
    }

    /*check : whether popup-layer is started or not */
    if( FALSE == pPopup_prv_struct->isLayerRegStatus)
    {
        barkway_service_emit_error_msg (pObject, pArg_app_name, " popup-layer not yet started ");
        return FALSE;
    }

    show_popup_internal_handle(pArg_app_name,inArg_popup_type,pArg_title,pArg_display_text,pArg_priority,pArg_popup_action,
    		pArg_image_data,dArg_timeout,pArg_sound,pArg_voice_control_time,pUser_data);

    barkway_service_complete_show_popup( pObject, pInvocation );
    return TRUE;
}


static void
show_selection_popup_internal_handle (const gchar *pArg_app_name,
                                      const gchar *pArg_title,
                                      GVariant *pArg_display_text,
                                      GVariant *pArg_rows_info,
                                      GVariant *pArg_image_data,
                                      gpointer pUser_data )
{
    PopupInfoStruct *pPopupInfoStruct = g_new0 (PopupInfoStruct, 1);
    GVariantBuilder *pDisplay_text = NULL;
    GVariantIter inCount;
    gchar *pValue = NULL;
    gchar *pKey = NULL;

    GVariantBuilder *pRowValues = NULL;
    GVariantIter inIncrementor;
    GVariant *pHashTable = NULL;
    GVariantBuilder *pImageValues = NULL;
    GVariantIter inCre;

    pPopupInfoStruct->app_name   = g_strdup (pArg_app_name);
    pPopupInfoStruct->title      = g_strdup (pArg_title);
    pPopupInfoStruct->popup_type = BARKWAY_POPUP_TYPE_ENUM_FOR_OTHER_TYPE ;

		pDisplay_text = g_variant_builder_new( G_VARIANT_TYPE ("a{ss}") );
		g_variant_iter_init ( &inCount, pArg_display_text);
		while( g_variant_iter_next (&inCount, "{ss}", &pKey, &pValue) )
		{
			 g_variant_builder_add (pDisplay_text, "{ss}", pKey, pValue);
		}
		pPopupInfoStruct->display_text = g_variant_builder_end (pDisplay_text);
	    g_variant_builder_unref(pDisplay_text);

	    //selection popup has default priority as MEDIUM
	    pPopupInfoStruct->priority = POPUP_ENUM_MEDIUM_PRIORITY;

		/*Building the hash table for rows_info in selection popup */

		pRowValues = g_variant_builder_new( G_VARIANT_TYPE ("aa{ss}") );

		g_variant_iter_init ( &inIncrementor, pArg_rows_info );
	    while ( (pHashTable = g_variant_iter_next_value (&inIncrementor)) != NULL)
	    {
	      g_variant_builder_add_value (pRowValues, pHashTable);
	      g_variant_unref (pHashTable);
	    }
		pPopupInfoStruct->rows_info = g_variant_builder_end ( pRowValues );
	    g_variant_builder_unref( pRowValues );

	    /*Building the hash table for images data*/
		pImageValues = g_variant_builder_new( G_VARIANT_TYPE ("a{ss}") );
		g_variant_iter_init( &inCre, pArg_image_data );
		while( g_variant_iter_next (&inCre, "{ss}", &pKey, &pValue) )
		{
			 g_variant_builder_add ( pImageValues, "{ss}", pKey, pValue );
		}
		pPopupInfoStruct->image_data = g_variant_builder_end( pImageValues );
	    g_variant_builder_unref( pImageValues );

	    pPopupInfoStruct->state_machine_status = POPUP_ENUM_CREATE;

	    /*Pushes the data into the queue*/
	     g_async_queue_push( pPopup_prv_struct->popup_waiting_queue, pPopupInfoStruct );
}



/*callback for selection popup show, when client requested to service */
static gboolean g_popup_handle_selection_popup( BarkwayService *pObject,
                                 GDBusMethodInvocation *pInvocation,
                                 const gchar *pArg_app_name,
                                 const gchar *pArg_title,
                                 GVariant *pArg_display_text,
                                 GVariant *pArg_rows_info,
                                 GVariant *pArg_image_data,
                                 gpointer pUser_data )
{
    POPUP_SERVICE_DEBUG_PRINT( " \n %s %d \n",__FUNCTION__,__LINE__ );

    /*check: if queue creation is not done then request is not handled*/
    if( NULL == pPopup_prv_struct->popup_waiting_queue )
    {
        return FALSE;
    }

    /*check : whether popup-layer is started or not */
    if( FALSE == pPopup_prv_struct->isLayerRegStatus)
    {
        barkway_service_emit_error_msg (pObject, pArg_app_name, " popup-layer not yet started ");
        POPUP_SERVICE_DEBUG_PRINT( " \n %s %d \n",__FUNCTION__,__LINE__ );
        return FALSE;
    }

    show_selection_popup_internal_handle(pArg_app_name,pArg_title,
    		pArg_display_text,pArg_rows_info,pArg_image_data,pUser_data);
     barkway_service_complete_selection_popup( pObject, pInvocation );
    return TRUE;
}


/*callback for update selection popup , when client requested to service */
static gboolean g_popup_handle_update_selection_popup_rows( BarkwayService *pObject,
                                 GDBusMethodInvocation *pInvocation,
                                 const gchar *pArg_app_name,
                                 const gchar *pArg_title,
                                 GVariant *pArg_rows_info,
                                 gboolean  arg_row_remove,
                                 gpointer pUser_data )
{
    GVariantBuilder *pRowValues = NULL;
    GVariantIter inIncrementor;
    GVariant *pHashTable = NULL;
    GVariant *pUpdateRowInfo = NULL;

    POPUP_SERVICE_DEBUG_PRINT( " \n %s %d \n",__FUNCTION__,__LINE__ );

    /*check: if queue creation is not done then request is not handled*/
    if( NULL == pPopup_prv_struct->popup_waiting_queue )
    {
        return FALSE;
    }

    /*check : whether popup-layer is started or not */
    if( FALSE == pPopup_prv_struct->isLayerRegStatus)
    {
        barkway_service_emit_error_msg (pObject, pArg_app_name, " popup-layer not yet started ");
        return FALSE;
    }

	/*Building the hash table for rows_info in selection popup */

	pRowValues = g_variant_builder_new( G_VARIANT_TYPE ("aa{ss}") );

	g_variant_iter_init ( &inIncrementor, pArg_rows_info );
    while ( (pHashTable = g_variant_iter_next_value (&inIncrementor)) != NULL)
    {
      g_variant_builder_add_value (pRowValues, pHashTable);
      g_variant_unref (pHashTable);
    }
    pUpdateRowInfo = g_variant_builder_end (pRowValues);
    g_variant_builder_unref( pRowValues );

    barkway_service_complete_update_selection_popup_rows( pObject, pInvocation );
    barkway_layer_emit_update_display_selection_popup (pPopup_prv_struct->popup_layer_obj,
                                                            g_strdup(pArg_app_name),
                                                            g_strdup(pArg_title),
                                                            pUpdateRowInfo,
                                                            arg_row_remove);
    return TRUE;
}

/*callback for hide popup,when client requested to service */
static gboolean g_popup_handle_hide ( BarkwayService *pObject, GDBusMethodInvocation *pInvocation,
                            const gchar *pArg_app_name, const gchar *pArg_title, gpointer pUser_data)
{
    guint inCount = 0;

    POPUP_SERVICE_DEBUG_PRINT( "\n %s %d \n",__FUNCTION__,__LINE__ );
    barkway_service_complete_hide_popup( pObject, pInvocation );

    /*check: if queue creation is not done then request is not handled*/
    if( NULL == pPopup_prv_struct->popup_waiting_queue )
        return FALSE;

    /*check : whether popup-layer is started or not */
    if( FALSE == pPopup_prv_struct->isLayerRegStatus)
    {
         barkway_service_emit_error_msg (pObject, pArg_app_name, " popup-layer not yet started ");
         return FALSE;
    }

    /*checking the title(serves as unique id)in list ,where all the request info is maintained */
    for(inCount=0; inCount<g_list_length(pPopup_prv_struct->request_info_list); inCount++)
    {
        PopupInfoStruct *pPopupInfoStruct = ( PopupInfoStruct *)g_list_nth_data( pPopup_prv_struct->request_info_list, inCount );
        if(!g_strcmp0( pPopupInfoStruct->title, pArg_title ))
        {
            pPopupInfoStruct->state_machine_status = POPUP_ENUM_HIDE_START;
            /*Pushes the data into the queue*/
            g_async_queue_push( pPopup_prv_struct->popup_waiting_queue, pPopupInfoStruct );
            break;
        }
    }
    return TRUE;
}

gpointer p_popup_service_thread( gpointer pUser_data )
{
    while (1)
    {
        /*Pops data from the queue. This function blocks until data become available*/
        PopupInfoStruct *pPopupInfoStruct = ( PopupInfoStruct *)g_async_queue_pop( pPopup_prv_struct->popup_waiting_queue );
        if( NULL != pPopupInfoStruct)
        {
            /*poped information is given to state machine*/
            v_popup_state_machine_handler( pPopupInfoStruct );
        }
    }
    return NULL;
}

static gboolean g_popup_service_initialization( void )
{
    GError *error = NULL;
    g_autoptr (CbyComponentIndex) component_index = NULL;

    /*variable initialization */
    pPopup_prv_struct->currentItem_priority = POPUP_ENUM_INVALID_PRIORITY;
    pPopup_prv_struct->current_state        = POPUP_ENUM_INITAL;
    pPopup_prv_struct->isLayerRegStatus     = FALSE;
    pPopup_prv_struct->pPopupInfo = NULL;
    pPopup_prv_struct->pCurrentAppOnTop =NULL;
    pPopup_prv_struct->internetStatus = FALSE;
    pPopup_prv_struct->pInvocation=NULL;
    pPopup_prv_struct->entry_point_index = NULL;
    /*initialize a mutex that has been allocated on the stack*/
    g_mutex_init( &pPopup_prv_struct->mutex_lock );

    /* Create entry point index */
    component_index = cby_component_index_new (CBY_COMPONENT_INDEX_FLAGS_NONE, &error);
    if (error)
      {
        POPUP_SERVICE_DEBUG_PRINT ("%s: code %d: %s", g_quark_to_string (error->domain),
                                   error->code, error->message);
        g_error_free (error);
      }
    else
      {
        pPopup_prv_struct->entry_point_index = cby_entry_point_index_new (component_index);
      }

    /*create message queue for popup requests */
    pPopup_prv_struct->popup_waiting_queue = g_async_queue_new();
    if(  NULL == pPopup_prv_struct->popup_waiting_queue )
       exit (0);

    return TRUE;
}
#if 0
static void v_popup_layer_on_name_acquired ( GDBusConnection *pConnection, const gchar *pName, gpointer pUser_data )
{
     POPUP_SERVICE_DEBUG_PRINT( "\n popup_layer_on_name_acquired \n" );
}

static void v_popup_layer_on_name_lost ( GDBusConnection *pConnection, const gchar *pName, gpointer pUser_data )
{
     POPUP_SERVICE_DEBUG_PRINT( "\n popup_layer_on_name_lost callback\n" );
}
#endif
static void v_popup_layer_on_bus_acquired ( GDBusConnection *pConnection, const gchar *pName, gpointer pUser_data )
{
  POPUP_SERVICE_DEBUG_PRINT( "\n popup_layer_on_bus_acquired %s \n", pName );
  pPopup_prv_struct->popup_layer_obj = barkway_layer_skeleton_new();

  /*response methods from popup-layer */
  g_signal_connect ( pPopup_prv_struct->popup_layer_obj, "handle-confirmation-result-list", G_CALLBACK (g_popup_handle_confirmation_result_list),pPopup_prv_struct);
  g_signal_connect ( pPopup_prv_struct->popup_layer_obj, "handle-confirmation-result", G_CALLBACK (g_popup_handle_confirmation_result),pPopup_prv_struct);
  g_signal_connect ( pPopup_prv_struct->popup_layer_obj, "handle-popup-status",        G_CALLBACK (g_popup_handle_popup_status),       pPopup_prv_struct);
  g_signal_connect ( pPopup_prv_struct->popup_layer_obj, "handle-registration-status", G_CALLBACK (g_popup_handle_registration_status),pPopup_prv_struct);
  g_signal_connect ( pPopup_prv_struct->popup_layer_obj, "handle-error-msg",           G_CALLBACK (g_popup_handle_error_msg),          pPopup_prv_struct);

  if (!g_dbus_interface_skeleton_export ( G_DBUS_INTERFACE_SKELETON ( pPopup_prv_struct->popup_layer_obj ),pConnection, "/org/apertis/Barkway/Layer", NULL))
  {
    g_warning( "\n export error \n" );
  }
}

static void v_popup_service_on_name_acquired (  GDBusConnection *pConnection, const gchar *pName, gpointer pUser_data )
{
     POPUP_SERVICE_DEBUG_PRINT( "\n popup_service_on_name_acquired \n" );
}

static void v_popup_service_on_name_lost ( GDBusConnection *pConnection, const gchar *pName, gpointer pUser_data )
{
     POPUP_SERVICE_DEBUG_PRINT( "\n popup_service_on_name_lost callback\n" );
}

static void v_app_mgr_proxy_clb( GObject *pSource_object,GAsyncResult *pRes,gpointer pUser_data)
{
    GError *error = NULL;

	POPUP_SERVICE_DEBUG_PRINT("\n  v_app_mgr_proxy_clb \n");
    pPopup_prv_struct->popup_app_mgr_obj = canterbury_app_manager_proxy_new_finish(pRes , &error);
    if(pPopup_prv_struct->popup_app_mgr_obj == NULL)
    {
    	POPUP_SERVICE_DEBUG_PRINT("error %s \n",g_dbus_error_get_remote_error(error));
    	return;
    }
    g_signal_connect (pPopup_prv_struct->popup_app_mgr_obj,
                        "notify::current-active-app",
                        G_CALLBACK (on_notify_current_app_state),
                        pUser_data);
}

static void v_app_mgr_name_appeared (GDBusConnection *pConnection,const gchar *pName,const gchar *pName_owner,gpointer user_data)
{
	 POPUP_SERVICE_DEBUG_PRINT( "\n v_app_mgr_name_appeared  \n" );
    canterbury_app_manager_proxy_new(pConnection,
                                         G_DBUS_PROXY_FLAGS_NONE,
                                         "org.apertis.Canterbury",
                                         "/org/apertis/Canterbury/AppManager",
                                         NULL,
                                         v_app_mgr_proxy_clb,
                                         NULL);
}

static void v_app_mgr_name_vanished(GDBusConnection *pConnection,const gchar *pName,gpointer pUser_data)
{
    if(NULL != pPopup_prv_struct->popup_app_mgr_obj)
    {
        g_object_unref(pPopup_prv_struct->popup_app_mgr_obj);
        pPopup_prv_struct->popup_app_mgr_obj = NULL;
    }
}


static void v_popup_service_on_bus_acquired ( GDBusConnection *pConnection, const gchar *pName, gpointer pUser_data )
{
  POPUP_SERVICE_DEBUG_PRINT( "\n popup_service_on_bus_acquired %s \n", pName );
  pPopup_prv_struct->popup_obj = barkway_service_skeleton_new();

#if 0
  /*registering to popup-layer */
  gint inPopup_layer_id;
  inPopup_layer_id = g_bus_own_name ( G_BUS_TYPE_SESSION,              /* bus type */
                                      "org.apertis.Barkway.Layer",             /* interface name */
                                      G_BUS_NAME_OWNER_FLAGS_NONE,     /* bus own flag, can be used to take away the bus and give it to another service */
                                      v_popup_layer_on_bus_acquired,   /* callback invoked when the bus is acquired */
                                      v_popup_layer_on_name_acquired,  /* callback invoked when interface name is acquired */
                                      v_popup_layer_on_name_lost,      /* callback invoked when name is lost to another service or other reason */
                                      NULL,                            /* user data */
                                      NULL);                           /* user data free func */
#endif
  v_popup_layer_on_bus_acquired(pConnection,pName,pUser_data);
    g_bus_watch_name (G_BUS_TYPE_SESSION,
						  "org.apertis.Canterbury",
						  G_BUS_NAME_WATCHER_FLAGS_AUTO_START,
						  v_app_mgr_name_appeared,
						  v_app_mgr_name_vanished,
						  NULL,
						  NULL);

  /* signals for PopupService methods */
  g_signal_connect ( pPopup_prv_struct->popup_obj, "handle-show-popup", G_CALLBACK (g_popup_handle_show), pPopup_prv_struct );
  g_signal_connect ( pPopup_prv_struct->popup_obj, "handle-show-blocking-popup", G_CALLBACK (g_popup_handle_show_blocking_popup), pPopup_prv_struct );
  g_signal_connect ( pPopup_prv_struct->popup_obj, "handle-hide-popup", G_CALLBACK (g_popup_handle_hide), pPopup_prv_struct );
  g_signal_connect ( pPopup_prv_struct->popup_obj, "handle-selection-popup", G_CALLBACK (g_popup_handle_selection_popup), pPopup_prv_struct );
  g_signal_connect ( pPopup_prv_struct->popup_obj, "handle-update-selection-popup-rows",G_CALLBACK (g_popup_handle_update_selection_popup_rows),pPopup_prv_struct);

  if ( !g_dbus_interface_skeleton_export ( G_DBUS_INTERFACE_SKELETON (pPopup_prv_struct->popup_obj), pConnection, "/org/apertis/Barkway/Service", NULL))
  {
    g_warning( "\n export error \n" );
  }
}

void _popup_show_cb(GObject *SearchObj,const gchar *arg_app_name,
  	  	  gint64 arg_popup_type,const gchar *arg_title,GVariant *arg_display_text,
  	  	  const gchar *arg_priority,GVariant *arg_popup_action,GVariant *arg_image_data,
  	  	  gdouble arg_timeout,const gchar *arg_sound,gint arg_voice_control_time,gpointer pUserData)
{
	 show_popup_internal_handle(arg_app_name,arg_popup_type,arg_title,arg_display_text,arg_priority,arg_popup_action,
			 arg_image_data,arg_timeout,arg_sound,arg_voice_control_time,pUserData);

}


void _selection_popup_show_cb(GObject *SearchObj,const gchar *arg_app_name,
	  	  const gchar *arg_title,GVariant *arg_display_text,
	  	   GVariant *arg_popup_action,GVariant *arg_image_data,
	  	  gpointer pUserData)
{
g_print("n_________________________\n%s %s %dn_________________________\n",__FILE__ , __FUNCTION__ , __LINE__);
	show_selection_popup_internal_handle(arg_app_name,arg_title,arg_display_text,arg_popup_action,
			 arg_image_data, pUserData);
}

void _popup_hide_cb(GObject *SearchObj,const gchar *arg_app_name, const gchar *pArg_title,gpointer pUserData)
{
    guint inCount = 0;

    /*check: if queue creation is not done then request is not handled*/
    if( NULL == pPopup_prv_struct->popup_waiting_queue )
        return ;

    for(inCount=0; inCount<g_list_length(pPopup_prv_struct->request_info_list); inCount++)
    {
        PopupInfoStruct *pPopupInfoStruct = ( PopupInfoStruct *)g_list_nth_data( pPopup_prv_struct->request_info_list, inCount );
        if(!g_strcmp0( pPopupInfoStruct->title, pArg_title ))
        {
            pPopupInfoStruct->state_machine_status = POPUP_ENUM_HIDE_START;
            /*Pushes the data into the queue*/
            g_async_queue_push( pPopup_prv_struct->popup_waiting_queue, pPopupInfoStruct );
            break;
        }
    }
}

/*
popup-service startup function.
Launched as a separate daemon during system initialization.
Initializes the D-BUS and establishes the connection for client.
Initializes the popupservice for client access.
*/
gint main( gint argc, gchar *argv[] )
{
    GMainLoop *pMainLoop = NULL;
    gint inPopup_srv_id;
 //   g_type_init();

    /* allocating memory for structure and initialized to 0's*/
    pPopup_prv_struct = g_new0( PopupPrvStruct, 1 );
    g_popup_service_initialization();

    /*registering to popup-service */
    inPopup_srv_id = g_bus_own_name ( G_BUS_TYPE_SESSION,               /* bus type */
                                      "org.apertis.Barkway",            /* interface name */
                                      G_BUS_NAME_OWNER_FLAGS_NONE,      /* bus own flag, can be used to take away the bus and give it to another service*/
                                      v_popup_service_on_bus_acquired,  /* callback invoked when the bus is acquired */
                                      v_popup_service_on_name_acquired, /* callback invoked when interface name is acquired */
                                      v_popup_service_on_name_lost,     /* callback invoked when name is lost to another service or other reason */
                                      NULL,                             /* user data */
                                      NULL);                            /* user data free func */

	//v_popup_plugin_dl_open();

    POPUP_SERVICE_DEBUG_PRINT( "\n PopupService has been initialised \n" );
    pMainLoop = g_main_loop_new ( NULL, FALSE );

    g_main_loop_run ( pMainLoop );
    g_clear_object (&pPopup_prv_struct->entry_point_index);
    g_bus_unown_name( inPopup_srv_id );
    return 0;
}
