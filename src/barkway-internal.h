/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include <glib.h>
#include <gio/gio.h>
#include <assert.h>
#include <float.h> // for FLT_MAX
#include <glib.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <canterbury/canterbury-platform.h>
#include <canterbury/gdbus/canterbury.h>
#include "barkway-enums.h"
#include "org.apertis.Barkway.Layer.h"
#include "barkway.h"


#define  POPUP_SERVICE_DEBUG_PRINT(...) //g_print( __VA_ARGS__)

typedef struct _PopupPrvStruct PopupPrvStruct;
typedef struct _PopupInfoStruct PopupInfoStruct;

typedef enum _PopupState PopupState ;
typedef enum _PopupPriority PopupPriority ;

enum _PopupState
{
    POPUP_ENUM_INITAL=0,
    POPUP_ENUM_CREATE ,
    POPUP_ENUM_SHOW_START,
    POPUP_ENUM_SHOW_END,
    POPUP_ENUM_HIDE_START,
    POPUP_ENUM_HIDE_END,
    POPUP_ENUM_CANCELLED,
    POPUP_ENUM_WAIT_STATE
};

enum _PopupPriority
{
  POPUP_ENUM_INVALID_PRIORITY=0,
  POPUP_ENUM_LOW_PRIORITY ,
  POPUP_ENUM_MEDIUM_PRIORITY ,
  POPUP_ENUM_HIGH_PRIORITY
};

struct _PopupPrvStruct
{
	BarkwayService *popup_obj;
    BarkwayLayer *popup_layer_obj;
    CbyEntryPointIndex *entry_point_index;
    CanterburyAppManager *popup_app_mgr_obj;
    GAsyncQueue *popup_waiting_queue;
    GMutex mutex_lock;
    gboolean recorderStatus;
    gboolean playerStatus;
    gboolean isPopupLayerReg;
    gboolean isLayerRegStatus;
    gchar *pCurrentAppOnTop;
    GList *request_info_list;
    PopupPriority currentItem_priority;
    PopupState current_state;
    const gchar *currentItem_title;
    PopupInfoStruct *pPopupInfo;
    gboolean internetStatus;
    GHashTable *module_list;
    GDBusMethodInvocation *pInvocation;
};

struct _PopupInfoStruct
{
    const gchar *app_name;
    gint64 popup_type;
    const gchar *title;
    GVariant *display_text;
    GVariant *popup_action;
    GVariant *rows_info;
    GVariant *image_data;
    gdouble timeout;
    const gchar *sound;
    gint voice_control_time;
    PopupPriority priority;
    PopupState state_machine_status;
};

void v_popup_plugin_dl_open (void );
void v_popup_plugin_free_module (gpointer userData) ;
gpointer popup_plugin_dl_find_func(GModule *module,const gchar* name);

void _popup_show_cb(GObject *SearchObj,const gchar *arg_app_name,
  	  	  gint64 arg_popup_type,const gchar *arg_title,GVariant *arg_display_text,
  	  	  const gchar *arg_priority,GVariant *arg_popup_action,GVariant *arg_image_data,
  	  	  gdouble arg_timeout,const gchar *arg_sound,gint arg_voice_control_time,gpointer pUserData);

void _selection_popup_show_cb(GObject *SearchObj,const gchar *arg_app_name,
	  	  const gchar *arg_title,GVariant *arg_display_text,
	  	   GVariant *arg_popup_action,GVariant *arg_image_data,
	  	  gpointer pUserData);

void _popup_hide_cb(GObject *SearchObj,const gchar *arg_app_name, const gchar *pArg_title,gpointer pUserData);

