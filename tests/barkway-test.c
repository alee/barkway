/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include <canterbury/gdbus/canterbury.h>
#include <glib.h>
#include <glib/gstdio.h>
#include <stdio.h>
#include <stdlib.h>
#include <gio/gio.h>

#include "org.apertis.Barkway.Service.h"
#define  POPUP_DEBUG_PRINT(...)  g_print( __VA_ARGS__)
static BarkwayService *barkwayservice_proxy = NULL;



static gboolean g_popup_status_clb( BarkwayService *pObject, const gchar *pArg_app_name, const gchar *pArg_title,
                          const gchar *pArg_popup_status, gpointer pUser_data )

{
    /* call back function for popup status response */
    POPUP_DEBUG_PRINT( "pArg_app_name = %s \n pArg_title =%s \n pArg_popup_status = %s \n",pArg_app_name,pArg_title,pArg_popup_status );
    return TRUE;
}

static gboolean g_confirmation_result_clb( BarkwayService *pObject, const gchar *pArg_app_name, const gchar *pArg_title,
                                 const gchar *pArg_confirmation_result, gpointer pUser_data )
{
    /* call back function for confirmation response */
    POPUP_DEBUG_PRINT( "pArg_app_name = %s \n pArg_title =%s \n  pArg_confirmation_result = %s \n",
                                                        pArg_app_name, pArg_title, pArg_confirmation_result );
    return TRUE;
}

static gboolean g_error_msg_clb( BarkwayService *pObject, const gchar *pArg_app_name, const gchar *pArg_error_msg, gpointer pUser_data )
{
    /* call back function for confirmation response */
    g_warning( "pArg_app_name = %s \n pArg_error_msg = %s \n",pArg_app_name, pArg_error_msg );
    return TRUE;
}

static void v_show_popup_clb( GObject *pSource_object, GAsyncResult *pRes, gpointer pUser_data)
{

    /* call back function for show dbus-method */
    GError *pError = NULL;
    gboolean bReturn_val = barkway_service_call_show_popup_finish( barkwayservice_proxy, pRes, &pError );

      if(bReturn_val == FALSE)
      {
         gchar *pError_msg = g_dbus_error_get_remote_error(pError);
         g_warning("pError %s \n",pError_msg);
         g_free(pError_msg);
         g_dbus_error_strip_remote_error(pError);
         g_warning("pError %s \n",pError->message);
         return;
      }
}

static void v_popup_update_selection_popup_clb( GObject *pSource_object, GAsyncResult *pRes, gpointer pUser_data)
{
    /* call back function for show dbus-method */
    GError *pError = NULL;
    gboolean bReturn_val = barkway_service_call_update_selection_popup_rows_finish( barkwayservice_proxy, pRes, &pError );

      if( FALSE == bReturn_val)
      {
         gchar *pError_msg = g_dbus_error_get_remote_error(pError);
         g_warning("pError %s \n",pError_msg);
         g_free(pError_msg);
         g_dbus_error_strip_remote_error(pError);
         g_warning("pError %s \n",pError->message);
         return;
      }
      else
      {
          POPUP_DEBUG_PRINT(" got the result \n");
      }
}


static void v_popup_show_selection_popup_clb( GObject *pSource_object, GAsyncResult *pRes, gpointer pUser_data)
{
    /* call back function for show dbus-method */
    GError *pError = NULL;
    gboolean bReturn_val = barkway_service_call_selection_popup_finish( barkwayservice_proxy, pRes, &pError );

      if(bReturn_val == FALSE)
      {
         gchar *pError_msg = g_dbus_error_get_remote_error(pError);
         g_warning("pError %s \n",pError_msg);
         g_free(pError_msg);
         g_dbus_error_strip_remote_error(pError);
         g_warning("pError %s \n",pError->message);
         return;
      }
      else
      {
          g_print(" got the result \n");
      }
}

static void v_popup_hide_clb( GObject *pSource_object, GAsyncResult *pRes, gpointer pUser_data)
{
    /* call back function for hide dbus-method */
    GError *pError = NULL;
    gboolean bReturn_val =barkway_service_call_hide_popup_finish( barkwayservice_proxy, pRes, &pError);
    if(bReturn_val == FALSE)
    {
        gchar *pError_msg = g_dbus_error_get_remote_error(pError);
        g_warning("pError %s \n",pError_msg);
        g_free(pError_msg);
        g_dbus_error_strip_remote_error(pError);
        g_warning("pError %s \n",pError->message);
        return;
    }
}

static gchar *p_getdata(void)
{
    /*function to get entered data*/
    gchar *pMsg = NULL;
    size_t inLen = 0;
    ssize_t inRead;

    setbuf(stdin,NULL);
    while ((inRead = getline( &pMsg, &inLen, stdin )) != -1)
    {
    	g_strdelimit(pMsg,"\n",'\0');
        return pMsg;
    }
    g_strdelimit(pMsg,"\n",'\0');
    return pMsg;
}

static gint
in_select_popup_type (void)
{
    /*function to select the popup type */
    gint inPopup_type = 1;
    /*different types of popup types */
    gchar *pPopType = p_getdata();
    inPopup_type = atoi(pPopType);

    return inPopup_type;
}

static gchar *
p_popup_priority (void)
{
    /*function to select the priority for request*/
    gchar *pPriority = NULL;
    gint inPopup_prio =0;
    gchar *pPrioValue = p_getdata();

    inPopup_prio = atoi(pPrioValue);

    switch(inPopup_prio)
    {
        case 1:
            pPriority = g_strdup("LOW");
            break;
        case 2:
            pPriority = g_strdup("MEDIUM");
            break;
        case 3:
            pPriority = g_strdup("HIGH");
            break;
        default:
        {
            g_print("enter value is not proper so LOW priority is taken as default \n");
            pPriority = g_strdup("LOW");
            break;
        }
    }
    return pPriority;
}


static GVariant *
p_enter_key_values (gint inIter)
{
    gint inCount =1;
    GVariantBuilder *pGvb = g_variant_builder_new(G_VARIANT_TYPE ("a{ss}"));
    GVariant *pHash = NULL;

    for(inCount=1;inCount<=inIter;inCount++)
    {
        gchar *pKey = NULL;
        gchar *pValue = NULL;

        g_print("(field%d : [key->Text-Active/Text-Passive/Image] and [value%d->vaild string/Image Path] \n ",inCount,inCount);
        g_print("\nEnter the key for field%d:",inCount);
        pKey = p_getdata();

        g_print("\nEnter the value for field%d:",inCount);
        pValue = p_getdata();

        g_variant_builder_add ( pGvb, "{ss}",pKey, pValue );

    }
    pHash = g_variant_builder_end (pGvb);
    g_variant_builder_unref(pGvb);
    return pHash;
}


static GVariant *
p_get_array_of_hash (void)
{
    gint inPopup_prio = 0;
    GVariant *pHash =NULL;
    GVariant *pArray_hash =NULL;

    const char *pMyReply = "Which type of row you want 2-field/3-field \n"
                           "Enter (1) for 2-field row \n"
                           "      (2) for 3-field row";

    GVariantBuilder *pGvb = g_variant_builder_new (G_VARIANT_TYPE_ARRAY);
    guint inNext_query = 0;

    g_variant_builder_init (pGvb, G_VARIANT_TYPE_ARRAY);

    g_print( "\n  %s \n ", pMyReply );
    if(!scanf("%d", &inPopup_prio))
    g_warning("no input!!!");
    	out:
    switch(inPopup_prio)
    {
        case 1:
             pHash = p_enter_key_values(3);
            break;
        case 2:
             pHash = p_enter_key_values(4);
            break;
        default:
            pHash = p_enter_key_values(3);
    }

    g_variant_builder_add_value (pGvb,pHash);

    /*for next row information*/
    g_print("\n Enter (1) for next row inforamtion  or (0)to exit : ");
    if(!scanf( "%d", &inNext_query ))
    g_warning("no input!!!");
    if( 1 == inNext_query)
        goto out;

    pArray_hash = g_variant_builder_end (pGvb);
    g_variant_builder_unref(pGvb);

    return pArray_hash;
}

static GVariant *
p_get_hash_values (void)
{
    /*function for entering key value pair into hash map*/
    GVariantBuilder *pGvb = g_variant_builder_new ( G_VARIANT_TYPE ("a{ss}") );
    gboolean gDisplay_msg = TRUE;
    gboolean gContinous_hashmap = FALSE;
    gchar *pArg_key1 = NULL;
    gchar *pArg_value1 = NULL;

    if( FALSE == gContinous_hashmap )
    {
        gchar *pKey = NULL;
        gchar *pValue = NULL;

        g_print("\nEnter the key :");
        pKey = p_getdata ();
        g_strdelimit(pKey,"\n",'\0');

        g_print("\nEnter the value :");
        pValue = p_getdata ();
        g_strdelimit(pKey,"\n",'\0');
        g_variant_builder_add ( pGvb, "{ss}",pKey, pValue );
        g_print("\nEnter (c) for More Hashmap values or Enter (q) to quit :");
        gContinous_hashmap = TRUE;
    }

    pArg_key1 = g_new0 (gchar, 200);
    pArg_value1 = NULL;
    while( TRUE == gContinous_hashmap  )
    {
        setbuf(stdin,NULL);
        if( FALSE == gDisplay_msg )
            g_print( "\nEnter (c) for More Hashmap values or Enter (q) to quit :");
        if(!scanf( "%s", pArg_key1 ))
           g_warning("no input!!!");
        if (!g_strcmp0(pArg_key1,"q"))
            break;

        else if (!g_strcmp0( pArg_key1,"c" ))
        {
            g_print("\nEnter the key : ");
            if(!scanf( "%s", pArg_key1 ))
             g_warning("no input!!!");

            g_print("\nEnter the value : ");
            pArg_value1 = p_getdata();
            g_strdelimit(pArg_value1,"\n",'\0');
            g_variant_builder_add (pGvb, "{ss}", pArg_key1, pArg_value1 );
        }
        gDisplay_msg = FALSE;
        continue;
    }

    return g_variant_builder_end (pGvb);
}

static void v_popup_show_req (GObject *pSource_object,GAsyncResult *pRes,gpointer pUser_data)
{
    gchar *pApp_name = NULL;
    gint inPopup_type;
    gchar *pTitle = NULL;
    gchar *pPriority = NULL;

    GCancellable *pCancellable = NULL;
    GVariant *pDisplay_Text = NULL;
    GVariant *pPopup_action = NULL;
    GVariant *pImage_info = NULL;
    gdouble dTimeout = 0.0;
    gchar *pSound = NULL;
    gint voicetime = 0.0;
    guint inNext_query = 0;

    out:
    dTimeout = 0.0;
    voicetime = 0.0;
    inNext_query = 0;

    g_print("\nEnter the AppName :");
    pApp_name = p_getdata ();

    g_print("\nEnter the popupType :");
    inPopup_type = in_select_popup_type ();

    g_print(" \n Enter the title (Which serves as a unique identifier):");
    pTitle = p_getdata ();
    g_strdelimit(pTitle,"\n",'\0');
    g_print(" \n Enter the display Text :key->Text-Active/Text-Passive/Text-Underline/Image ,value->dispalyText \n");
    pDisplay_Text = p_get_hash_values () ;

    g_print(" \n Enter the priority: \n");
    pPriority  = p_popup_priority ();

    g_print(" \n Enter the action button info key->BUTTON..n, value->OK/CANCEL/YES/NO/ANY-STRING \n");
    pPopup_action = p_get_hash_values () ;

    g_print(" \n Enter the image info: key->AppIcon/MsgIcon  value->imageFilePath \n");
    pImage_info = p_get_hash_values () ;

    g_print(" \n Enter the timeout (or) mention -1 for default \n");
    if(!scanf( "%lf", &dTimeout ))
    g_warning("no input!!!");

    g_print("\nEnter the sound file path (or) send default as a string for default: ");
    pSound = p_getdata ();
    g_print(" \n Enter the voice time (or) mention -1 for default \n");
       if(!scanf( "%d", &voicetime ))
         g_warning("no input!!!");

    /*D-Bus method call to show popup */
    barkway_service_call_show_popup(barkwayservice_proxy,pApp_name,inPopup_type,pTitle,pDisplay_Text,pPriority,pPopup_action,
                                         pImage_info,dTimeout,pSound,voicetime,pCancellable,v_show_popup_clb,pUser_data);

    /*for next query to request to show popup*/
    g_print("\n Enter (1) for next show or (0)to exit : ");
    if(!scanf( "%d", &inNext_query ))
     g_warning("no input!!!");
    if( 1 == inNext_query)
        goto out;
}

static void v_selection_popup_show_req (GObject *pSource_object,GAsyncResult *pRes,gpointer pUser_data)
{
    GCancellable *pCancellable = NULL;
    gchar *pApp_name = NULL;
    gchar *pTitle = NULL;
    GVariant *pDisplay_Text = NULL;
    GVariant *pRows_info = NULL;
    GVariant *pImage_info = NULL;
    guint inNext_query = 0;

    out:
    inNext_query = 0;

    g_print("\nEnter the AppName :");
    pApp_name = p_getdata ();

    g_print(" \n Enter the title (Which serves as a unique identifier):");
    pTitle = p_getdata ();

    g_print(" \n Enter the display Text :key->Text-Active/Text-Passive/Text-Underline/Image ,value->dispalyText \n");
    pDisplay_Text = p_get_hash_values () ;

    g_print(" \n Enter the selection popup rows information \n");
    pRows_info = p_get_array_of_hash () ;

    g_print(" \n Enter the image info: key->AppIcon/MsgIcon  value->imageFilePath \n");
    pImage_info = p_get_hash_values () ;


    /*D-Bus method call to show popup */
    barkway_service_call_selection_popup(barkwayservice_proxy,pApp_name,pTitle,pDisplay_Text,pRows_info,
                                         pImage_info,pCancellable,v_popup_show_selection_popup_clb,pUser_data);

    /*for next query to request to show popup*/
    g_print("\n Enter (1) for next show or (0)to exit : ");
    if(!scanf( "%d", &inNext_query ))
       g_warning("no input!!!");
    if( 1 == inNext_query)
        goto out;
}


static void v_selection_popup_update_req (GObject *pSource_object,GAsyncResult *pRes,gpointer pUser_data)
{
    GCancellable *pCancellable = NULL;
    gchar *pApp_name = NULL;
    gchar *pTitle = NULL;
    GVariant *pRows_info = NULL;
    guint inNext_query = 0;

    out:
    inNext_query = 0;

    g_print("\nEnter the AppName :");
    pApp_name = p_getdata ();

    g_print(" \n Enter the title (Which serves as a unique identifier):");
    pTitle = p_getdata ();

    g_print(" \n Enter the rows information to update the popup \n");
    pRows_info = p_get_array_of_hash () ;

    barkway_service_call_update_selection_popup_rows (barkwayservice_proxy,pApp_name,pTitle,pRows_info,FALSE,pCancellable,
                                                           v_popup_update_selection_popup_clb,pUser_data);

    /*for next query to request to update selection popup*/
    g_print("\n Enter (1) for next show or (0)to exit : ");
    if(!scanf( "%d", &inNext_query ))
       g_warning("no input!!!");
    if( 1 == inNext_query)
        goto out;
}

static void v_popup_hide_req (GObject *pSource_object,GAsyncResult *pRes,gpointer pUser_data)
{
    GCancellable *pCancellable = NULL;
    gchar *pApp_name = NULL;
    gchar *pTitle = NULL;
    guint inNext_query = 0;

    out:
    inNext_query = 0;

    g_print("\nEnter the AppName :");
    pApp_name = p_getdata ();

    g_print(" \n Enter the title (Which serves as a unique identifier):");
    pTitle = p_getdata ();

    barkway_service_call_hide_popup ( barkwayservice_proxy, pApp_name, pTitle, pCancellable, v_popup_hide_clb, pUser_data);

    g_print("\n Enter (1) for next request or (0)to exit : ");
    if(!scanf( "%d", &inNext_query ))
       g_warning("no input!!!");
    if( 1 == inNext_query)
        goto out;
}

static void v_popup_client_proxy_clb( GObject *pSource_object, GAsyncResult *pRes, gpointer pUser_data)
{
    GError *pError;
    gint inApi_type =0;

    /* finishes the proxy creation and gets the proxy ptr */
    barkwayservice_proxy = barkway_service_proxy_new_finish( pRes, &pError );

    if(NULL == barkwayservice_proxy)
        g_warning("\n Error intializing the proxy");

    g_signal_connect(barkwayservice_proxy,"popup-status",(GCallback)g_popup_status_clb,pUser_data);
    g_signal_connect(barkwayservice_proxy,"confirmation-result",(GCallback)g_confirmation_result_clb,pUser_data);
    g_signal_connect(barkwayservice_proxy,"error-msg",(GCallback)g_error_msg_clb,pUser_data);

    out:
    g_print("\n Enter (0) for ShowPopup \n (1) for selection popup \n (2) for HidePopup \n (3) for update selection popup\
    choose any one from above options To send request:");

    if(!scanf( "%d", &inApi_type ))
       g_warning("no input!!!");

    switch( inApi_type )
    {
        case 0:
            v_popup_show_req( pSource_object, pRes, pUser_data); /*fun called when to show popup*/
            break;

        case 1:
            v_selection_popup_show_req( pSource_object, pRes, pUser_data); /*fun called when to hide popup*/
            break;

        case 2:
            v_popup_hide_req( pSource_object, pRes, pUser_data); /*fun called when to hide popup*/
            break;

        case 3:
            v_selection_popup_update_req( pSource_object, pRes, pUser_data); /*fun called when to update popup*/
            break;


        default:
            goto out;
            break;
    }
}

/* Asynchronously creates a proxy for the D-Bus interface */
static gpointer p_popup_service_proxy_new(gpointer pUser_data)
{
    GError *pError;

    /*Creates a new proxy for a remote interface exported by a connection on a message bus*/
    barkway_service_proxy_new ((GDBusConnection *)pUser_data,
                                    G_DBUS_PROXY_FLAGS_NONE,
                                    "org.apertis.Barkway",
                                    "/org/apertis/Barkway/Service",
                                    NULL,
                                    v_popup_client_proxy_clb,
                                    &pError);
    return FALSE;
}

static void v_popup_name_appeared ( GDBusConnection *pConnection, const gchar *pName, const gchar *pName_owner,gpointer pUser_data )
{
    g_print("%s \n",__FUNCTION__);
    g_timeout_add(400,( GSourceFunc )p_popup_service_proxy_new, pConnection);
}

static void v_popup_name_vanished( GDBusConnection *pConnection, const gchar *pName, gpointer pUser_data )
{
    g_print("%s \n",__FUNCTION__);
    if( NULL != barkwayservice_proxy )
        g_object_unref( barkwayservice_proxy ); /*Decreases the ref cnt if obj.When its ref cnt drops to 0,the object is finalized*/
}

gint main( gint argc, gchar *argv[] )
{
    GMainLoop *pMainLoop = NULL;
  //  g_type_init();
    /*thread system Initialization*/


    /* Starts watching name on the bus specified by bus_type */
    /* calls name_appeared and name_vanished when the name is known to have a owner or known to lose its owner */
   // gint inPopup_srv_watch;
    g_bus_watch_name ( G_BUS_TYPE_SESSION,
                                        "org.apertis.Barkway",
                                        G_BUS_NAME_WATCHER_FLAGS_AUTO_START,
                                        v_popup_name_appeared,
                                        v_popup_name_vanished,
                                        NULL,
                                        NULL);

    pMainLoop = g_main_loop_new ( NULL, FALSE );
    g_main_loop_run ( pMainLoop );
    exit (0);
}
