# Barkway

Barkway service is a service responsible for handling global popup management
framework.

## Overview

This D-BUS interface is responsible for system wide popup management, i.e
displaying, hiding,cancelling popups and all the popups are shown on popup
layer which is on top of all the applications.

Barkway service implements asynchronous queue mechanism to push all popup
requests in queue and popups are shown based on the priorities set.

## Contact

[Mail the maintainers](mailto:maintainers@lists.apertis.org)
